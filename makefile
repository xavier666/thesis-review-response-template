FILE=main
READER=zathura

all:
	@pdflatex ${FILE}.tex
	#@bibtex ${FILE}.aux
	@pdflatex ${FILE}.tex
	@pdflatex ${FILE}.tex

show:
	@${READER} ${FILE}.pdf &

clean:
	@rm -f ${FILE}.pdf || true
	@rm -f ${FILE}.log || true
	@rm -f ${FILE}.aux || true
	@rm -f ${FILE}.out || true
	@rm -f ${FILE}.bbl || true
	@rm -f ${FILE}.blg || true
	@rm -f ${FILE}.synctex.gz || true
	@rm -f ${FILE}.idx || true
